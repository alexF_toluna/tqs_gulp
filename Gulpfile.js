const gulp = require('gulp');
const _ = require('lodash');
const utils = require('./projects/utils');

const projectsApp = require('./webapp/app');
const projectsState = projectsApp.getState().projects;
const allProjectsConfigs = projectsApp.getAllProjectsConfigs();
let activeProjects = [];
let watchTasks = [];
let startupTasks = [];


Object.keys(projectsState).filter((name, idx) => {
   if(projectsState[name].watch === true || projectsState[name].startup === true){
       activeProjects.push(name);

       if(projectsState[name].watch === true){
           watchTasks.push(allProjectsConfigs[name]['watch']);
       }

       if(projectsState[name].startup === true){
           startupTasks = startupTasks.concat(allProjectsConfigs[name]['startup']);
       }
   }

    /*if(projectsState[name].startup === true){
        startupTasks = startupTasks.concat(allProjectsConfigs[name]['startup']);
        activeProjects.push(name);
    }*/
});

console.log(watchTasks);
console.log(startupTasks);

if(activeProjects.length > 0){
    activeProjects.forEach((name) => {
        allProjectsConfigs[name].init();
    });

    gulp.task('global_watch', watchTasks, () => {
        console.log('global_watch finished');
    });

    gulp.task('global_startup', startupTasks, () => {
       console.log('global_startup finished');
    });

    return;
}

gulp.task('default', [], () => {
    console.log('Default finished');
});
