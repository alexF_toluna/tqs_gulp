#!/usr/bin/env node

const gulp = require('gulp');
const ejs = require('gulp-ejs');
const moment = require('moment');
const path = require('path');
const fs = require('fs');
const gutil = require('gulp-util');
const rename = require('gulp-rename');

const configsDir = path.resolve(path.join(__dirname, 'configs'));
const TQS_ROOT = path.resolve('D:/Dev/TQS/src');

//console.log(process.env);

const getFileName = (name) => {
    let rv = name.replace(/\./g, '_').toLowerCase();
    let counter = 0;
    let duplicate = false;

    if (fs.existsSync(path.join(configsDir, name + '.js'))) {
        console.log('File ' + path.join(configsDir, name + '.js') + ' exists');
        return null;
    }


    //console.log('FileName: ', rv + '.js');

    return name + '.js';
};

const deleteConfig = (name) => {
    try {
        const allConfigs = getAllProjectsConfigs();
        const fileName = allConfigs[name]['fileName'];

        fs.unlinkSync(fileName);

        return true;
    } catch (err) {
        return false;
    }
};

const deleteAllConfigs = () => {
    try {
        const allConfigs = getAllProjectsConfigs();
        Object.keys(allConfigs).forEach((name) => {
            fs.unlinkSync(allConfigs[name]['fileName']);
        });

        return true;
    } catch (err) {
        return false;
    }
};

const createConfig = (params, cb) => {
    if (typeof cb !== 'function') {
        cb = () => {
        };
    }
    if (!params || typeof params !== 'object' || !params.projectDir) {
        cb(false);
        return false;
    }

    params.userName = require("os").userInfo().username;
    params.date = moment().format('DD/MM/YYYY HH:mm:ss');


    const fileName = getFileName(params.fileName || params.projectDir);

    if (!fileName) {
        cb(false);
        return false;
    }

    if (!params.projectName) {
        params.projectName = params.projectDir;
    }

    if(!params.projectDescription){
        params.projectDescription = '';
    }

    if(params.projectName === 'TQS_MVC.Wizard.Define'){
        params.projectDescription = 'Default config. The project is inside TQS_MVC';
    }

    //console.log('Creating config: ', fileName, params);

    let tplFileName = params.projectName === 'TQS_MVC.Wizard.Define' ? "TQS_MVC.Wizard.Define-config-template.js"  : "config-template.js";
    let done = false;

    gulp.src(path.join(__dirname, tplFileName))
        .pipe(ejs(params).on('error', (err) => {
            gutil.log(err);
            cb(false);
        }))
        .pipe(rename(fileName))
        .pipe(gulp.dest(path.join(__dirname, "configs")))
        .on('end', () => {
            if(!done){
                cb(true);
                done = true;
            }
        })
        .on('finish', () => {
            if(!done){
                cb(true);
                done = true;
            }
        });
};

const createInitialConfigs = (cb) => {
    console.log('Creating initial configs');
    let filteredDirs = getInitialProjectsList();

    //console.log(filteredDirs);
    let cbArray = [];
    filteredDirs.forEach((dirName) => {
        createConfig({projectDir: dirName, projectDescription: 'Default config'}, (result) => {
            cbArray.push(result);
            if(cbArray.length === filteredDirs.length){
                if(cb){
                    cb(true);
                }
            }
        });
    });
};

const getInitialProjectsList = () => {
    const rootDir = TQS_ROOT;
    const rootDirFiles = fs.readdirSync(rootDir);
    let filteredDirs = [];
    filteredDirs = rootDirFiles.filter((fileName) => {
        const filePath = path.join(rootDir, fileName);
        const fStat = fs.statSync(filePath);

        return fStat.isDirectory() &&
            fileName !== 'TQS_MVC' &&
            fileName.indexOf('TQS_MVC.') === 0 &&
            (
                fileName.indexOf('TQS_MVC.Runtime') === 0 ||
                fileName.indexOf('TQS_MVC.Question') === 0 ||
                fileName.indexOf('TQS_MVC.Wizard') === 0 ||
                fileName.indexOf('TQS_MVC.Common') === 0
            ) &&
            fs.existsSync(path.join(filePath, 'MVCScripts')) &&
            fs.existsSync(path.join(filePath, 'Asset')) &&
            fs.existsSync(path.join(filePath, 'Views'));
    });

    return filteredDirs;
};

const getAllProjectsConfigs = () => {
    const configsDir = path.resolve(__dirname, 'configs');
    const rootDirFiles = fs.readdirSync(configsDir);
    let rv = {};
    rootDirFiles.forEach((configFileName) => {
        let config = require(path.join(configsDir, configFileName));
        rv[config.projectName] = config;
        rv[config.projectName]['fileName'] = path.join(configsDir, configFileName);

    });
    return rv;
};

//createInitialConfigs();

//createConfig({projectDir: 'TQS_MVC.Wizard.Define'});

module.exports = {
    createConfig,
    deleteConfig,
    deleteAllConfigs,
    createInitialConfigs,
    getInitialProjectsList,
    getAllProjectsConfigs
};