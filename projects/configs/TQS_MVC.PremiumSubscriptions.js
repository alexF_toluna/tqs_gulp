/*
 TQS project config file
 created at 13/06/2017 08:35:52
 by alex.frolov

 Links:
 path module - https://nodejs.org/api/path.html
 fs module - https://nodejs.org/api/fs.html

 gulp - http://gulpjs.com/
 */
const path = require('path');
const fs = require('fs');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const gutil = require('gulp-util');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const utils = require('../utils'); 
const transpileSourceScript = utils.transpileSourceScript;

const projectName = 'TQS_MVC.PremiumSubscriptions';
const projectDir = 'TQS_MVC.PremiumSubscriptions';
const projectDescription = 'Default config';
const paths = utils.getDefaultProjectPaths(projectDir);

const copyScripts = (filePath) => {
    console.log('Copying scripts');
    const specificDest = getSpecificDest(filePath, paths.dirNames.scripts);
    transpileSourceScript(filePath)
        .pipe(gulp.dest(specificDest))
        .on('success', () => {
            console.log('Finished successfully');
        })
        .on('error', (e) => {
            console.log('ERROR: ', JSON.stringify(e))
        })
        .on('end', () => {
            console.log('Finished Copying scripts');
        });
};

const copyViews = (filePath) => {
    console.log('Copying views');
    const specificDest = getSpecificDest(filePath, paths.dirNames.views);
    gulp.src(filePath)
        .pipe(gulp.dest(specificDest))
        .on('success', () => {
            console.log('Finished successfully');
        })
        .on('error', (e) => {
            console.log('ERROR: ', JSON.stringify(e))
        })
        .on('end', () => {
            console.log('Finished Copying views');
        });
};

const copyAsset = (filePath) => {
    console.log('Copying assets');
    const specificDest = getSpecificDest(filePath, paths.dirNames.asset);
    gulp.src(filePath)
        .pipe(gulp.dest(specificDest))
        .on('success', () => {
            console.log('Finished successfully');
        })
        .on('error', (e) => {
            console.log('ERROR: ', JSON.stringify(e))
        })
        .on('end', () => {
            console.log('Finished Copying assets');
        });
};

const compileSass = (dir) => {
    console.log('Compiling SASS files');
    const srcDir = [
        path.join(dir, '**/*.scss'),
        '!' + path.join(dir, '**/Customize.scss'),
        '!' + path.join(dir, '**/rzslider.scss'),
        '!' + path.join(dir, '**/node_modules/*'),
    ];
    const specificDest = getSpecificDest(dir, paths.dirNames.asset);

    const autoprefixerOptions = {
        browsers: ['last 5 versions', '> 5%', 'Firefox ESR']
    };

    const sassOptions = {};

    gulp
        .src(srcDir)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(plumber())
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(specificDest))
        .pipe(gulp.dest(dir))
        .on('success', () => {
            console.log('Finished successfully');
        })
        .on('error', (e) => {
            console.log('ERROR: ', JSON.stringify(e))
        })
        .on('end', () => {
            console.log('Finished Compiling SASS ');
        });
};

const getSpecificDest = (filePath, subDir) => {
    const src = path.dirname(filePath);
    const subPath = src.substr(path.join(paths.SRC.projectRoot, subDir).length, src.length - path.join(paths.SRC.projectRoot, subDir).length);

    return path.join(path.join(paths.DEST.root, subDir), subPath);
};


const init = () => {
    gulp.task(projectName + '_copyScripts', () => {
        console.log('Copying scripts');
        return gulp.src(path.join(paths.SRC.projectScripts, '**/*'))
            .pipe(gulp.dest(paths.DEST.scripts));
    });

    gulp.task(projectName + '_copyViews', () => {
        console.log('Copying views');
        return gulp.src(path.join(paths.SRC.projectViews, '**/*'))
            .pipe(gulp.dest(paths.DEST.views));
    });

    gulp.task(projectName + '_copyAsset', (cb) => {
        console.log('Copying assets to ', paths.DEST.asset);
        return gulp.src([
            path.join(paths.SRC.projectAsset, '**/*'),
            '!' + path.join(paths.SRC.projectAsset, '**/*.scss')
        ])
            .pipe(gulp.dest(paths.DEST.asset));
    });

    gulp.task(projectName + '_compileSass', [projectName + '_copyAsset'], () => {
        console.log('Compiling sass');
        const autoprefixerOptions = {
            browsers: ['last 5 versions', '> 5%', 'Firefox ESR']
        };

        const sassOptions = {};

        const files = [
            path.join(paths.SRC.projectAsset, '**/*.scss'),
            '!' + path.join(paths.SRC.projectAsset, '**/Customize.scss'),
            '!' + path.join(paths.SRC.projectAsset, '**/rzslider.scss'),
            '!' + path.join(paths.SRC.projectAsset, '**/node_modules/*')
        ];

        return gulp
            .src(files)
            .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(sass(sassOptions).on('error', sass.logError))
            .pipe(plumber())
           .pipe(sourcemaps.write('.'))
            .pipe(autoprefixer(autoprefixerOptions))
            .pipe(gulp.dest(paths.DEST.asset));
    });

    gulp.task(projectName + '_watch', () => {
        console.log(projectName + ': watching project files');
        gulp.watch([
            path.join(paths.SRC.projectAsset, '**/*.scss'),
        ], (evt) => {
            console.log(evt.path, evt.type);
            compileSass(paths.SRC.projectAsset);
        });

        gulp.watch([
            paths.SRC.projectAsset,
            '!' + path.join(paths.SRC.projectAsset, '**/*.scss'),
        ], (evt) => {
            console.log(evt.path, evt.type);
            copyAsset(paths.SRC.projectAsset);
        });

        gulp.watch([path.join(paths.SRC.projectScripts, '**/*')], (evt) => {
            console.log(evt.path, evt.type);
            copyScripts(evt.path);
        });

        gulp.watch([path.join(paths.SRC.projectViews, '**/*')], (evt) => {
            console.log(evt.path, evt.type);
            copyViews(evt.path);
        });
    });
};


module.exports = {
    projectName,
    projectDescription,
    init: init,
    watch: projectName + '_watch',
    startup: [projectName + '_copyScripts', projectName + '_copyViews', projectName + '_compileSass']
};