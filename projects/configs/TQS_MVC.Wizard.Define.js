/*
 TQS project config file
 created at 13/06/2017 08:35:53
 by alex.frolov

 Links:
 path module - https://nodejs.org/api/path.html
 fs module - https://nodejs.org/api/fs.html

 gulp - http://gulpjs.com/
 */
const path = require('path');
const fs = require('fs');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const gutil = require('gulp-util');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const utils = require('../utils');

/***** NOTE ! TQS_MVC.Wizard.Define project is currently inside TQS_MVC directory ***/

const projectName = 'TQS_MVC.Wizard.Define';
const projectDir = 'TQS_MVC.Wizard.Define';
const projectDescription = 'Default config. The project is inside TQS_MVC';
const paths = utils.getDefaultProjectPaths(projectDir);

/*** OVERWRITE PATHS FOR Wizard.Define project. Only styles needed **/
const stylesPath = path.join(paths.DEST.root, 'Styles', 'Wizard');


const compileSass = (dir) => {
    console.log('Compiling SASS files in: ', stylesPath);
    const srcDir = [
        path.join(stylesPath, '*.scss')
    ];


    const autoprefixerOptions = {
        browsers: ['last 5 versions', '> 5%', 'Firefox ESR']
    };

    const sassOptions = {};

    gulp
        .src(srcDir)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(plumber())
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(stylesPath))
        .on('success', () => {
            console.log('Finished successfully');
        })
        .on('error', (e) => {
            console.log('ERROR: ', JSON.stringify(e))
        })
        .on('end', () => {
            console.log('Finished Compiling SASS ');
            console.log("Saved to: ", stylesPath);
        });
};


const init = () => {
    gulp.task(projectName + '_compileSass', [], () => {
        console.log('Compiling sass');
        const autoprefixerOptions = {
            browsers: ['last 5 versions', '> 5%', 'Firefox ESR']
        };

        const sassOptions = {};

        const files = [
            path.join(stylesPath, '*.scss')
        ];

        return gulp
            .src(files)
            .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(sass(sassOptions).on('error', sass.logError))
            .pipe(plumber())
           .pipe(sourcemaps.write('.'))
            .pipe(autoprefixer(autoprefixerOptions))
            .pipe(gulp.dest(stylesPath));
    });

    gulp.task(projectName + '_watch', () => {
        console.log(projectName + ': watching project files');
        gulp.watch([
            path.join(stylesPath, '**/*.scss'),
        ], (evt) => {
            console.log(evt.path, evt.type);
            compileSass(evt.path);
        });
    });
};


module.exports = {
    projectName,
    projectDescription,
    init: init,
    watch: projectName + '_watch',
    startup: [projectName + '_compileSass']
};