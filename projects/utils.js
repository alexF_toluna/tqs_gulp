const path = require('path');
const gutil = require('gulp-util');
const gulp = require('gulp');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');

let tqsRoot = path.resolve('D:/Dev/TQS/src/');
let destRoot = path.join(tqsRoot, 'TQS_MVC');
const scriptsName = 'MVCScripts';
const viewsName = 'Views';
const assetName = 'Asset';
const isBabelTranspilationEnabled = false;

const setTqsRoot = (tqsPath) => {
    tqsRoot = tqsPath;
    destRoot = path.join(tqsRoot, 'TQS_MVC');
};

const getDefaultProjectPaths = (dirname) => {
    let rv = {};


    const projectRoot = path.join(tqsRoot, dirname),
        scriptsSrc = path.join(projectRoot, scriptsName),
        viewsSrc = path.join(projectRoot, viewsName),
        assetSrc = path.join(projectRoot, assetName);

    const scriptsDest = path.join(destRoot, scriptsName),
        viewsDest = path.join(destRoot, viewsName),
        assetDest = path.join(destRoot, assetName);

    rv.SRC = {
        tqsRoot,
        projectRoot,
        projectScripts: scriptsSrc,
        projectViews: viewsSrc,
        projectAsset: assetSrc
    };

    rv.DEST = {
        root: destRoot,
        scripts: scriptsDest,
        views: viewsDest,
        asset: assetDest
    };

    rv.dirNames = {
        scripts: scriptsName,
        asset: assetName,
        views: viewsName
    };


    return rv;
};

const transpileSourceScript = (filePath) => {
    if (isBabelTranspilationEnabled) {
        console.log('Transpiling script...');
        return gulp.src(filePath)
            .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(babel({
                presets: [
                    "@babel/preset-env"
                ].map(require.resolve)
            }))
            .pipe(plumber())
            .pipe(sourcemaps.write('.'));
    } else {
        return gulp.src(filePath)
    }
};

module.exports = {
    setTqsRoot,
    getDefaultProjectPaths,
    transpileSourceScript
};