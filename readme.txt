- execute install.bat;
- execute start_app.bat (don`t close cmd);

On webpage:
*********************************

---------------------------------
Header:                         +
---------------------------------
"TQS Root" - ensure that the path appearing in a text input is a real path to your TQS repo(TO "src" FOLDER !).
If not, type in correct path and press "save" button next to input field.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

"New Config" button toggles "New Project Config" dialog. Project Directory is mandatory. Type in a name of directory inside "/src" directory.
Hit "Create" button - standart/default config will be created and added to the list below.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

"Create Initial Configs" - will create bunch of initial configs for most of tqs projects.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

----------------------------------------
Left Panel                             +
----------------------------------------

Select one or more projects to watch or execute startup scripts and hit "Save".
Hit "Run Watch" to begin "watch" on selected projects.
Hit "Run Startup" to execute startup scripts on selected projects.
Hit "delete" to delete config for project.


------------------------------------------
Right Panel                              +
------------------------------------------

Streaming output from gulp task runner and node`s shell.
Hover mouse cursor to right border of panel - message filter will appear.