const jsonfile = require('jsonfile');
const path = require('path');
const fs = require('fs');
const configs = require('../projects/configs');
const utils = require('../projects/utils');

let projects = {};
let state = null;

const init = () => {
    state = getState();
};

const getAllProjectsConfigs = () => {
    return configs.getAllProjectsConfigs();
};

const getState = (fromFile) => {
    if (!state || fromFile) {
        state = readStateFromFile();
    }

    const projectsState = state.projects;
    const allProjectsConfigs = getAllProjectsConfigs();
    let isDiff = false;

    //console.log('All configs: ', allProjectsConfigs);

    if (JSON.stringify(Object.keys(projectsState)) !== JSON.stringify(Object.keys(allProjectsConfigs))) {
        for (let i = 0; i < Object.keys(allProjectsConfigs).length; i++) {
            let projectName = Object.keys(allProjectsConfigs)[i];

            if (Object.keys(projectsState).indexOf(Object.keys(allProjectsConfigs)[i]) === -1) {
                isDiff = true;

                //console.log('Description: ', projectName);

                projectsState[projectName] = {
                    watch: false,
                    startup: false,
                    projectDescription: allProjectsConfigs[projectName]['projectDescription']
                }
            }

            if(!(typeof projectsState[projectName]['projectDescription'] === 'string')){
                isDiff = true;
                projectsState[projectName]['projectDescription'] = allProjectsConfigs[projectName]['projectDescription'];
            }
        }

        for (let i = 0; i < Object.keys(projectsState).length; i++) {
            if (Object.keys(allProjectsConfigs).indexOf(Object.keys(projectsState)[i]) === -1) {
                isDiff = true;
                delete projectsState[Object.keys(projectsState)[i]];
            }
        }
    }

    if (isDiff) {
        //console.log('State changed !');
        saveProjectsState(projectsState);
    }

    //console.log('State: ', state);

    return state;
};

const readStateFromFile = () => {
    const tqsRoot = process.env.TOLUNA_ROOT ? path.join(process.env.TOLUNA_ROOT, 'src') : path.resolve('D:/Dev/TQS/src');

    if (!fs.existsSync(path.join(__dirname, 'app-state.json'))) {
        jsonfile.writeFileSync(path.join(__dirname, 'app-state.json'), {
            tqsRoot: tqsRoot,
            projects: {}
        }, {spaces: 2});
    }

    let appState = jsonfile.readFileSync(path.join(__dirname, 'app-state.json'));

    if (!appState) {
        appState = {
            tqsRoot: tqsRoot,
            projects: {}
        };
    }

    if (!appState.projects) {
        appState.projects = {};
    }

    if (!appState.tqsRoot) {
        appState.tqsRoot = tqsRoot;
    }

    utils.setTqsRoot(appState.tqsRoot);

    return appState;
};

const saveProjectsState = (projects) => {
    state.projects = projects;
    saveState();
};

const saveTqsRoot = (path) => {
    if (!fs.existsSync(path)) {
        return false;
    }
    state.tqsRoot = path;
    saveState();

    return true;
};

const saveState = () => {
    jsonfile.writeFileSync(path.join(__dirname, 'app-state.json'), state, {spaces: 2});
};

const createConfig = (params, cb) => {
    configs.createConfig(params, (res) => {
        getState(true);
        console.log('app.js - Config created');
        cb(res);
    });
};

const deleteConfig = (name) => {
    const result = configs.deleteConfig(name);

    getState(true);
    return result;
};

const deleteAllConfigs = () => {
    const result = configs.deleteAllConfigs();
    state.projects = {};

    state = getState(false);
    saveState();
    return result;
};

const createInitialConfigs = (cb) => {
    configs.createInitialConfigs((result) => {
        state = getState(true);

        saveState();
        if(cb){
            cb(result);
        }else{
            return result;
        }
    });
};


module.exports = {
    init,
    getAllProjectsConfigs,
    getState,
    saveState,
    saveTqsRoot,
    saveProjectsState,
    createConfig,
    deleteConfig,
    deleteAllConfigs,
    createInitialConfigs
};