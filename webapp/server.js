/* IMPORTS */
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');
const child_process = require('child_process');
const opn = require('opn');
const WebSocket = require('ws');
const stripAnsi = require('strip-ansi');
const moment = require('moment');
/* END OF IMPORTS */


const port = 54345;
const app = express();
const projectsApp = require('./app');
const server = http.createServer(app);
const wss = new WebSocket.Server({server});
const ORIGIN_TYPES = {
    SERVER: 'Server',
    GULP: 'Gulp'
};

const MESSAGE_TYPES = {
    ERROR: 'ERROR',
    MESSAGE: 'Message'
};

let gulpProcess = null;


const killProcess = (processRef, cb) => {
    if (!processRef) {
        return;
    }

    require('child_process').exec('taskkill /PID ' + processRef.pid + ' /T /F', (err, data) => {
        if (cb) {
            cb(err, data);
        }
    });
    processRef = null;
};

const startGulpProcess = (taskName) => {
    const spawn = child_process.spawn;

    broadCastMessage('Launching Gulp process');

    gulpProcess = spawn('gulp', [taskName], {
        cwd: path.resolve(path.join(__dirname, '..')),
        env: process.env,
        shell: true
    });

    if (!wss) {
        return;
    }


    gulpProcess.stdout.on('data', (data) => {
        broadCastMessage(data, {origin: ORIGIN_TYPES.GULP});
    });

    gulpProcess.stderr.on('data', (data) => {
        broadCastMessage(data, {origin: ORIGIN_TYPES.GULP, type: MESSAGE_TYPES.ERROR});
    });

    gulpProcess.on('close', (code) => {
        broadCastMessage(code, {origin: ORIGIN_TYPES.GULP});
    });


    gulpProcess.on('error', (code) => {
        broadCastMessage(code, {origin: ORIGIN_TYPES.GULP});
    });
};

const broadCastMessage = (body, params = {}) => {
    if (!wss) {
        return;
    }

    let {origin, type, specificClient} = params;
    let ts = moment().format('HH:mm:ss');
    //let message = `[${ts}]${origin || ORIGIN_TYPES.SERVER} ${type || MESSAGE_TYPES.MESSAGE}: ${stripAnsi(body.toString('utf8'))}`;

    let cleanMsg = null;

    try {
        cleanMsg = stripAnsi(body.toString('utf8'));
    } catch (e) {
        cleanMsg = stripAnsi(JSON.stringify(e));

        console.error('Failed to stringify: ', body, typeof body);
        //console.log(e);
    }

    let jsonMsg = {
        body: cleanMsg,
        ts,
        origin: origin || ORIGIN_TYPES.SERVER,
        type: type || MESSAGE_TYPES.MESSAGE
    };


    wss.clients.forEach((client) => {
        if (!specificClient && client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify(jsonMsg));
        } else if (specificClient && specificClient === client) {
            client.send(JSON.stringify(jsonMsg));
        }
    });
};

process.on ('SIGINT', () => {
    //console.log ('\nCTRL+C...');
    killProcess(gulpProcess);
    console.log('OOPS ! EXIT CODE IS: ', 'SIGINT');
    process.exit (0);
});

process.on ('SIGTERM', () => {
    //console.log ('\nCTRL+C...');
    killProcess(gulpProcess);
    console.log('OOPS ! EXIT CODE IS: ', 'SIGTERM');
    process.exit (0);
});

process.on ('exit', () => {
    //console.log ('\nCTRL+C...');
    killProcess(gulpProcess);
    console.trace('OOPS ! EXIT CODE IS: ', 'exit');
    process.exit (0);
});



projectsApp.init();


app.use(morgan('dev'));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(express.static(__dirname + '/dist'));


app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'dist', 'index.html'));
});

app.post('/deleteProjectConfig', (req, res) => {
    const {name} = req.body;

    const result = projectsApp.deleteConfig(name);

    res.send(result);
});

app.post('/deleteAllConfigs', (req, res) => {
    const result = projectsApp.deleteAllConfigs();

    console.log('Configs deleted. Configs now: ', Object.keys(projectsApp.getState().projects).length);

    res.send(result);
});

app.post('/createConfig', (req, res) => {
    const {projectDir, fileName, projectDescription, projectName} = req.body;

    console.log('Creating config: ', projectDir, fileName);

    projectsApp.createConfig({projectDir, fileName, projectDescription, projectName}, (response) => {
        console.log('server.js - Config created, sending respoce to client: ', response);
        res.send(response);
    });
});

app.post('/createDefaultConfigs', (req, res) => {
    projectsApp.createInitialConfigs((result) => {
        res.send(result);
    });
});

app.post('/updateTqsRoot', (req, res) => {
    //console.log(req.body.path);
    if (!projectsApp.saveTqsRoot(req.body.path)) {
        res.send(false);
        return;
    }
    res.send(true);
});

app.post('/getAppState', (req, res) => {
    console.log('Getting app state');
    const appState = projectsApp.getState();
    res.send(appState);
});

app.post('/getProjectsList', (req, res) => {
    res.send(projectsApp.getState().projects);
});

app.post('/updateProjects', (req, res) => {
    if (gulpProcess) {
        killProcess(gulpProcess, (err, data) => {
            broadCastMessage('Gulp process stoped');
            projectsApp.saveProjectsState(req.body.projects);
            res.send(err === null);
        });
    } else {
        projectsApp.saveProjectsState(req.body.projects);
        res.send(true);
    }

    broadCastMessage('Configs saved');
});

app.post('/runTasks', (req, res) => {
    if (gulpProcess) {
        killProcess(gulpProcess);
        gulpProcess = null;
        broadCastMessage('Stoped Gulp process');
    }

    startGulpProcess('global_watch');

    res.send(true);
});

app.post('/stopTasks', (req, res) => {
    if (gulpProcess) {
        killProcess(gulpProcess);
        gulpProcess = null;
        broadCastMessage('Stoped Gulp process');
    }

    res.send(true);
});

app.post('/runStartup', (req, res) => {
    if (gulpProcess) {
        killProcess(gulpProcess);
        gulpProcess = null;
        broadCastMessage('Stoped Gulp process');
    }

    startGulpProcess('global_startup');

    res.send(true);
});

app.post('/stopStartup', (req, res) => {
    if (gulpProcess) {
        killProcess(gulpProcess);
        gulpProcess = null;
        broadCastMessage('Stoped Gulp process');
    }

    res.send(true);
});

app.post('/stopGulp', (req, res) => {
    if (gulpProcess) {
        killProcess(gulpProcess);
        gulpProcess = null;
        broadCastMessage('Stoped Gulp process');
    }else{
        broadCastMessage('Gulp process was not running');
    }

    res.send(true);
});


server.listen(port, (err) => {
    console.log('listening on port ', port);
    console.log('Opening dashboard...');

    if (!process.env.IS_DEV) {
        opn('http://localhost:' + port);
    }

});


wss.on('connection', (ws) => {
    console.log('Client connected to WS server');
    broadCastMessage('Welcome to TQS build system.', {client: ws});

    ws.on('message', (msg) => {
        console.log('WS received message from client: ', msg);
        broadCastMessage(msg);
    });
});

