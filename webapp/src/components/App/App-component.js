import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Router, Route, IndexRoute, Link, browserHistory} from 'react-router';
import styles from './styles.scss';

class AppComponent  extends Component{
    constructor(props){
        super(props);
        this.name = 'AppComponent';
    }

    componentDidMount(){

    }

    componentWillMount(){

    }

    render(){
        //console.log('Rendering App: ', this.props);

        const getComponent = () => {
            const {children} = this.props;

            return React.cloneElement(children);
        };

        return (
            <div id="app" className="container">
                {getComponent()}
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);