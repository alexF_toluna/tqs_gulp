import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import ProjectsList from '../ProjectsList/ProjectsList-component';
import LoadingLayer from '../LoadingLayer/LoadingLayer-component';
import OutputPanel from '../OutputPanel/OutputPanel-component';
import classnames from 'classnames';
import axios from 'axios';
import toastr from 'toastr';
import NewConfigModal from '../NewConfigModal/NewConfigModal-component';
import moment from 'moment';
/*import CodeMirror from 'react-codemirror';
 import 'codemirror/mode/javascript/javascript';
 import 'codemirror/lib/codemirror.css';*/
import './styles.scss';


const ORIGIN_TYPES = {
    SERVER: 'Server',
    GULP: 'Gulp'
};

const MESSAGE_TYPES = {
    ERROR: 'ERROR',
    MESSAGE: 'Message'
};


class DashboardComponent extends Component {
    constructor(props) {
        super(props);
        this.name = 'DashboardComponent';

        toastr.options = {
            "newestOnTop": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 500,
            "timeOut": 1000,
            "extendedTimeOut": 1000,
            'preventDuplicates': true
        };

        this.state = {
            projects: {},
            allSelected: {
                watch: false,
                startup: false
            },
            tqsRoot: null,
            serverOutput: [],
            socketOpened: false,
            loading: {
                state: true,
                projects: true
            },
            modals: {
                newConfig: {
                    visible: false
                }
            }
        };

        this.wsConnTimer = null;

        this.createSocket = this.createSocket.bind(this);
        this.onSocketOpen = this.onSocketOpen.bind(this);
        this.onSocketError = this.onSocketError.bind(this);
        this.onSocketMessage = this.onSocketMessage.bind(this);
        this.onSocketClose = this.onSocketClose.bind(this);

        this.loadAppState = this.loadAppState.bind(this);

        this.getView = this.getView.bind(this);
        this.getModals = this.getModals.bind(this);
        this.getHeaderBlock = this.getHeaderBlock.bind(this);
        this.getChildrenComponents = this.getChildrenComponents.bind(this);
        this.toggleWatchState = this.toggleWatchState.bind(this);
        this.toggleStartupState = this.toggleStartupState.bind(this);
        this.getActiveProjectsCount = this.getActiveProjectsCount.bind(this);
        this.updateProjects = this.updateProjects.bind(this);

        this.stopGulp = this.stopGulp.bind(this);
        this.runTasks = this.runTasks.bind(this);
        this.stopTasks = this.stopTasks.bind(this);
        this.runStartup = this.runStartup.bind(this);
        this.stopStartup = this.stopStartup.bind(this);


        this.updateTqsRoot = this.updateTqsRoot.bind(this);
        this.toggleNewConfigForm = this.toggleNewConfigForm.bind(this);
        this.getContentBlock = this.getContentBlock.bind(this);
        this.showNewConfigForm = this.showNewConfigForm.bind(this);
        this.createNewConfig = this.createNewConfig.bind(this);
        this.deleteProjectConfig = this.deleteProjectConfig.bind(this);
        this.setLoadingState = this.setLoadingState.bind(this);
        this.createInitialConfigs = this.createInitialConfigs.bind(this);


        this.toggleAllConfigs = this.toggleAllConfigs.bind(this);
        this.deleteAllConfigs = this.deleteAllConfigs.bind(this);
        this.reloadConfigs = this.reloadConfigs.bind(this);
    }

    toggleAllConfigs(field) {
        if (!field || ['watch', 'startup'].indexOf(field) === -1) {
            field = 'watch';
        }
        let projects = {...this.state.projects};
        const flag = !this.state.allSelected[field];

        for (let i = 0; i < Object.keys(projects).length; i++) {
            projects[Object.keys(projects)[i]][field] = flag;
        }

        let allSelected = {...this.state.allSelected};
        allSelected[field] = flag;

        this.setState({projects, allSelected});

        this.updateProjects(projects);
    }

    deleteAllConfigs() {
        this.setLoadingState(true);

        axios.post('/deleteAllConfigs').then((res) => {
            this.reloadConfigs();
        }).catch((err) => {
            this.reloadConfigs();
        });
    }

    reloadConfigs() {
        this.setState({projects: {}});
        this.setLoadingState(true);
        this.loadAppState();
    }

    getModals() {
        if (this.state.modals.newConfig.visible) {
            return (
                <NewConfigModal
                    isOpen={this.state.modals.newConfig.visible}
                    onRequestClose={() => {
                        console.log('closing');
                        this.setState({modals: {newConfig: {visible: false}}})
                    }}
                    onFormSave={(data) => {
                        console.log('Saving: ', data);
                        this.createNewConfig(data);
                    }}/>
            );
        }
    }

    setLoadingState(value) {
        let loading = {...this.state.loading};
        loading.state = value;
        this.setState({loading});
    }

    deleteProjectConfig(project) {
        console.log('Deleting config: ', project);

        this.setLoadingState(true);
        axios
            .post('/deleteProjectConfig', {name: project})
            .then((res) => {
                console.log(res.data);
                toastr.success('Project config deleted');
                this.loadAppState();
            })
            .catch((err) => {
                console.error(err);
                toastr.error('Failed to delete config');
                this.setLoadingState(false);
            });
    }

    toggleNewConfigForm() {
        /*let newConfigForm = {...this.state.newConfigForm};
         newConfigForm.visible = !newConfigForm.visible;
         newConfigForm.projectDir = '';
         newConfigForm.fileName = '';
         this.setState({newConfigForm});*/
        this.setState({modals: {newConfig: {visible: !this.state.modals.newConfig.visible}}});
    }

    createInitialConfigs() {
        this.setLoadingState(true);

        axios
            .post('/createDefaultConfigs')
            .then((res) => {
                this.loadAppState();
            })
            .catch((err) => {
                console.log(err);
                toastr.error(err);
                this.setLoadingState(false);
            })
    }

    createNewConfig(data) {
        console.log(data);
        const {dirName, fileName, projName, projDescr} = data;
        //this.toggleNewConfigForm();
        const self = this;

        if (!dirName || dirName.length === 0) {
            toastr.error('Invalid Project Directory');

            return;
        }


        self.setLoadingState(true);

        let state = {...self.state};
        state.modals.newConfig.visible = false;
        self.setState(state);

        axios
            .post('/createConfig', {
                projectDir: dirName,
                fileName,
                projectName: projName,
                projectDescription: projDescr
            })
            .then((res) => {

                self.setLoadingState(false);


                console.log(res);
                if (res.data) {
                    toastr.success('Config created');

                    this.loadAppState();
                } else {
                    toastr.error('Failed to create config');
                    //this.setLoadingState(false);
                    self.setLoadingState(false);
                }
            })
            .catch((err) => {
                console.error(err);
                toastr.error('Failed to create config');
                self.setLoadingState(false);
            });
    }

    updateTqsRoot() {
        this.setLoadingState(true);
        axios.post('/updateTqsRoot', {path: this.state.tqsRoot}).then((res) => {
            console.log(res);
            if (res.data) {
                toastr.success('TQS Root updated');
            } else {
                toastr.error('Failed to update TQS Root');
            }

            this.setLoadingState(false);
        }).catch((err) => {
            console.error(err);
            toastr.error('Failed to update TQS Root');
            this.setLoadingState(false);
        });
    }

    createSocket() {
        const ws = window.WebSocket || window.MozWebSocket;
        this.socket = new ws('ws://' + window.location.hostname + ':' + window.location.port);
        this.socket.onopen = this.onSocketOpen;
        this.socket.onerror = this.onSocketError;
        this.socket.onmessage = this.onSocketMessage;
        this.socket.onclose = this.onSocketClose;
    }

    onSocketOpen() {
        console.log('Socket opened');
        this.setState({socketOpened: true});
        //this.socket.send("TEST FROM CLIENT");

        toastr.info('Connected to server');
    }

    onSocketClose() {
        //console.log('Socket closed');

        this.setState({socketOpened: false});

        //toastr.error('Disconnected from server');

        setTimeout(() => {
            this.createSocket();
        }, 1000);
    }

    onSocketError(error) {
        //console.error('Socket error: ', error);
    }

    onSocketMessage(msg) {
        //console.log('Socket message: ', msg.data);
        let out = this.state.serverOutput.slice();

        out.push(msg.data);

        if (out.length > 300) {
            out.shift();
        }

        this.setState({serverOutput: out});

        //console.log(out);
    }

    loadAppState(){
        this.setLoadingState(true);
        axios.post('/getAppState').then((res) => {
            console.log('AppState: ', res.data);
            toastr.success('Data loaded.');

            let allSelected = {
                watch: true,
                startup: true
            };

            Object.keys(res.data.projects).forEach((name) => {
                allSelected.watch = allSelected.watch && res.data.projects[name].watch;
                allSelected.startup = allSelected.startup && res.data.projects[name].startup;
            });

            let state = {...this.state};
            const {tqsRoot, projects} = res.data;
            state.tqsRoot = tqsRoot;
            state.projects = projects;
            state.allSelected = allSelected;

            this.setState(state);
            this.setLoadingState(false);
        }).catch((err) => {
            console.log(err);
            toastr.error(err);
            this.setLoadingState(false);
        });
    }

    componentDidMount() {
        this.createSocket();

        this.loadAppState();
    }

    componentWillUpdate(nextProps) {

    }

    componentDidUpdate() {
        setTimeout(() => {
            $(this.projectsListElementContainer)
                .find('> ul:first')
                .slimscroll({
                    height: '500px'
                });

            $(this.serverOutputElement).slimscroll({
                height: '500px',
                scrollTo: $(this.serverOutputElement)[0].scrollHeight
            });
        });
    }


    getChildrenComponents() {

        const {children} = this.props;

        return children ? React.cloneElement(children) : null;

    }

    updateProjects(customProjects) {
        const projects = customProjects ? customProjects : this.state.projects;
        //console.log('Updating projects: ', this.state.projects);
        this.setLoadingState(true);
        axios
            .post('/updateProjects', {projects})
            .then((response) => {
                console.log(response);
                toastr.success('Projects saved.');

                console.log('State: ', this.state);
                this.setLoadingState(false);
            })
            .catch((err) => {
                console.error(err);
                toastr.error('Failed to save projects.');
                this.setLoadingState(false);
            });
    }

    runTasks() {
        //this.setState({serverOutput: []});
        axios
            .post('/runTasks')
            .then((response) => {
                console.log(response);
            })
            .catch((err) => {
                console.error(err);
            });
    }

    stopTasks() {
        axios
            .post('/stopTasks')
            .then((response) => {
                console.log(response);
            })
            .catch((err) => {
                console.error(err);
            });
    }

    runStartup(){
        axios
            .post('/runStartup')
            .then((response) => {
                console.log(response);
            })
            .catch((err) => {
                console.error(err);
            });
    }

    stopStartup(){
        axios
            .post('/stopStartup')
            .then((response) => {
                console.log(response);
            })
            .catch((err) => {
                console.error(err);
            });
    }

    stopGulp(){
        axios
            .post('/stopGulp')
            .then((response) => {
                console.log(response);
            })
            .catch((err) => {
                console.error(err);
            });
    }

    getActiveProjectsCount() {
        return Object.keys(this.state.projects).filter((project) => {
            return this.state.projects[project].watch === true;
        }).length;
    }

    toggleWatchState(name) {
        let projects = {...this.state.projects};
        projects[name]['watch'] = !projects[name]['watch'];

        let allSelected = {...this.state.allSelected};
        allSelected.watch = true;

        Object.keys(projects).forEach((name) => {
            allSelected.watch = allSelected.watch && projects[name].watch;
        });

        this.setState({
            projects,
            allSelected
        });

        this.updateProjects(projects);
    }

    toggleStartupState(name) {
        let projects = {...this.state.projects};
        projects[name]['startup'] = !projects[name]['startup'];

        let allSelected = {...this.state.allSelected};
        allSelected.startup = true;

        Object.keys(projects).forEach((name) => {
            allSelected.startup = allSelected.startup && projects[name].startup;
        });

        this.setState({
            projects,
            allSelected
        });

        this.updateProjects(projects);
    }

    showNewConfigForm() {
        if (!this.state.newConfigForm.visible) {
            return null;
        }
        return (
            <div className="container-fluid new-config-form-container">
                <div className="panel panel-primary">
                    <div className="panel-heading">New Project Config</div>
                    <div className="panel-body">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="form">
                                    <div className="form-group">
                                        <label htmlFor="" class="required">Project Directory</label>
                                        <input type="text"
                                               className="form-control input-sm"
                                               value={this.state.newConfigForm.projectDir} onChange={(evt) => {
                                            let formState = {...this.state.newConfigForm};
                                            formState.projectDir = evt.target.value;
                                            this.setState({newConfigForm: formState});
                                        }}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="">File Name</label>
                                        <input type="text"
                                               className="form-control input-sm"
                                               value={this.state.newConfigForm.fileName} onChange={(evt) => {
                                            let formState = {...this.state.newConfigForm};
                                            formState.fileName = evt.target.value;
                                            this.setState({newConfigForm: formState});
                                        }}/>
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-primary" onClick={() => {
                                            this.createNewConfig();
                                        }}>Create
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getContentBlock() {
        let rowClass = classnames({
            'row': true,
            'content-block': true,
            /*'hidden': this.state.newConfigForm.visible*/
        });

        const toggleWatchBtnClasses = classnames({
            'btn btn-sm btn-toggle-watch-all': true,
            'btn-default': !this.state.allSelected.watch,
            'btn-primary': this.state.allSelected.watch
        });

        const toggleStartupBtnClasses = classnames({
            'btn btn-sm btn-toggle-startup-all': true,
            'btn-default': !this.state.allSelected.startup,
            'btn-primary': this.state.allSelected.startup
        });

        let canRunWatch = false;
        let canRunStartup = false;
        for (let i = 0; i < Object.keys(this.state.projects).length; i++) {
            canRunWatch = canRunWatch || this.state.projects[Object.keys(this.state.projects)[i]]['watch'];
            canRunStartup = canRunStartup || this.state.projects[Object.keys(this.state.projects)[i]]['startup'];
        }

        return (
            <div className={rowClass}>
                <div className="col-md-7 left-panel">
                    <div className="row">
                        <div className="col-xs-10 projects-panel-header">
                            <div className="h4">Projects watches
                                ({this.getActiveProjectsCount()}/ {Object.keys(this.state.projects).length}
                                active)
                            </div>
                            <div className="projects-panel-buttons-wrapper">
                                <button onClick={(evt) => {
                                    this.reloadConfigs();
                                }} title="Reload configs" className="btn btn-sm btn-primary btn-reload-configsl"><i
                                    className="fa fa-refresh"> </i></button>
                                <button onClick={(evt) => {
                                    this.deleteAllConfigs();
                                }} title="Delete all configs" className="btn btn-sm btn-danger btn-delete-all"><i
                                    className="fa fa-close"> </i></button>
                                <button onClick={(evt) => {
                                    this.toggleAllConfigs('startup');
                                }} title="Toggle startup state for all configs"
                                        className={toggleStartupBtnClasses}><b>S</b></button>
                                <button onClick={(evt) => {
                                    this.toggleAllConfigs('watch');
                                }} title="Toggle watch state for all configs"
                                        className={toggleWatchBtnClasses}><b>W</b>{/*<i
                                 className="fa fa-check"> </i>*/}</button>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-10" ref={(element) => {
                            this.projectsListElementContainer = element;
                        }}>
                            <ProjectsList projects={this.state.projects}
                                          onWatchToggle={this.toggleWatchState}
                                          onStartupToggle={this.toggleStartupState}
                                          onProjectDelete={this.deleteProjectConfig}/>
                        </div>
                        <div className="col-xs-2 buttons-container">
                            {/*<button className="btn btn-default" onClick={() => {
                                this.updateProjects();
                            }}>Save Configs
                            </button>
                            <hr/>*/}
                            <button {...{disabled: !canRunWatch}} className="btn btn-primary" onClick={() => {
                                this.runTasks();
                            }}>Run Watch
                            </button>


                            <button {...{disabled: !canRunStartup}} className="btn btn-primary" onClick={() => {
                                this.runStartup();
                            }}>Run Startup
                            </button>

                            <hr/>

                            <button className="btn btn-warning" onClick={() => {
                                console.log('Can run tasks: ', canRunStartup);
                                this.stopGulp();
                            }}>Stop Gulp
                            </button>
                        </div>
                    </div>
                    <div className="row">

                    </div>
                </div>
                <div className="col-md-5 right-panel">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="h4">&nbsp;</div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <OutputPanel connected={this.state.socketOpened} data={this.state.serverOutput}
                                         setElement={(element) => {
                                             this.serverOutputElement = element;
                                         }}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getHeaderBlock() {
        return (
            <div className="row header-block">
                <div className="">
                    <div className="form-inline root-dir-form">
                        <label htmlFor="" className="form-label">TQS Root:</label>
                        <input type="text" className="form-control input-sm"
                               value={this.state.tqsRoot || ''} onChange={(e) => {
                            console.log(e.target.value)
                            this.setState({tqsRoot: e.target.value});
                        }}/>
                        <button className="btn btn-sm btn-primary" onClick={(e) => {
                            this.updateTqsRoot();
                        }}>
                            <i className="fa fa-check"> </i>
                        </button>
                    </div>
                </div>
                <div className="">
                    <button className="btn btn-primary btn-sm" onClick={() => {
                        this.toggleNewConfigForm()
                    }}>New Config
                    </button>
                    {/*{this.showNewConfigForm() }*/}
                </div>
                <div>
                    <button className="btn btn-primary btn-sm" onClick={() => {
                        this.createInitialConfigs();
                    }}>Create Initial Configs
                    </button>
                </div>
            </div>
        );
    }

    getView() {
        const wrapperClasses = classnames({
            'dashboard-wrapper container-fluid': true,
            'hidden': this.state.loading.state === true
        });
        //console.log('State: ', this.state);

        return (
            <div id="dashboard-view" class="container">
                <LoadingLayer show={this.state.loading.state}/>
                <div className={wrapperClasses}>
                    <div className="dashboard-content container-fluid">
                        {this.getHeaderBlock()}
                        {this.getContentBlock()}
                    </div>
                </div>
                {this.getModals()}
            </div>
        );
    }

    render() {
        //console.log('State: ', this.state);
        return (
            this.getView()
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardComponent);