import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import './styles.scss';

const LoadingLayerComponent = (props) => {
    const {show} = props;

    if (!show) {
        return null;
    }

    return (
        <div className="loading-layer-wrapper">
            <div className="loading-layer-container">
                <div className="loading-layer-content">
                    <div className="image-block">

                    </div>
                    <strong>Loading, please wait...</strong>
                </div>
            </div>
        </div>
    );
};

module.exports = LoadingLayerComponent;

LoadingLayerComponent.propTypes = {
    show: PropTypes.bool.isRequired
};