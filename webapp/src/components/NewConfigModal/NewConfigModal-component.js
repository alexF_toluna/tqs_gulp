import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Modal from 'react-modal';
import './styles.scss';

const FIELD_NAMES = {
    DIRECTORY_NAME: 'DIRECTORY_NAME',
    PROJECT_NAME: 'PROJECT_NAME',
    PROJECT_DESCRIPTION: 'PROJECT_DESCRIPTION',
    FILE_NAME: 'FILE_NAME'
};

let modalStyle = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.75)',
        zIndex: 99
    },
    content: {}
};

let modalClassName = 'new-config-modal';

function getParent() {
    return document.querySelector('body');
}

class NewConfigModal extends Component {
    constructor(props) {
        super(props);
        this.name = 'NewConfigModal';
        this.state = {
            fields: {
                [FIELD_NAMES.DIRECTORY_NAME]: {
                    value: '',
                    error: null
                },
                [FIELD_NAMES.PROJECT_NAME]: {
                    value: '',
                    error: null
                },
                [FIELD_NAMES.PROJECT_DESCRIPTION]: {
                    value: '',
                    error: null
                },
                [FIELD_NAMES.FILE_NAME]: {
                    value: '',
                    error: null
                }
            }
        };

        this.onModalOpened = this.onModalOpened.bind(this);
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.onFieldChange = this.onFieldChange.bind(this);

        console.log('boom');
    }

    onFieldChange(fieldName, evt) {
        //console.log(fieldName, evt.target.value);
        let fieldsCopy = {...this.state.fields};
        fieldsCopy[fieldName].value = evt.target.value;


        this.setState({fields:fieldsCopy});
    }

    close(evt) {
        this.props.onRequestClose();
    }

    save(evt) {
        let fields = {
            dirName: null,
            projName: null,
            projDescr: null
        };

        const dirName = this.state.fields[FIELD_NAMES.DIRECTORY_NAME].value,
            projName = this.state.fields[FIELD_NAMES.PROJECT_NAME].value,
            fileName = this.state.fields[FIELD_NAMES.FILE_NAME].value,
            projDescr = this.state.fields[FIELD_NAMES.PROJECT_DESCRIPTION].value;

        let stateCopy = {...this.state.fields};

        if (!dirName || dirName.length === 0) {
            stateCopy[FIELD_NAMES.DIRECTORY_NAME].error = 'Enter name';
            this.setState({
                fields: stateCopy
            });

            return;
        }

        this.props.onFormSave({
            dirName,
            fileName,
            projName,
            projDescr
        });
    }

    onModalOpened() {
        console.log('Opened');
        this.dirNameElement.focus();
    }

    render() {
        //console.log('rendering: ', this.state.fields);
        const inputClasses = {
            [FIELD_NAMES.DIRECTORY_NAME]: classnames({
                'form-group': true,
                'error': this.state.fields[FIELD_NAMES.DIRECTORY_NAME].error
            }),
            [FIELD_NAMES.PROJECT_NAME]: classnames({
                'form-group': true,
                'error': this.state.fields[FIELD_NAMES.PROJECT_NAME].error
            }),
            [FIELD_NAMES.PROJECT_DESCRIPTION]: classnames({
                'form-group': true,
                'error': this.state.fields[FIELD_NAMES.PROJECT_DESCRIPTION].error
            }),
            [FIELD_NAMES.FILE_NAME]: classnames({
                'form-group': true,
                'error': this.state.fields[FIELD_NAMES.FILE_NAME].error
            })
        };
        return (

            <Modal
                parentSelector={getParent}
                isOpen={this.props.isOpen}
                onAfterOpen={this.onModalOpened}
                onRequestClose={this.props.onRequestClose}
                className={modalClassName}
                style={modalStyle}
                contentLabel={':)'}
            >
                <div className="panel panel-info">
                    <div className="panel-heading text-center">
                        <p><b>New Project Config</b></p>
                        <button className="btn btn-danger" onClick={(evt) => {
                            this.close(evt);
                        }}>
                            <b className="fa fa-close"> </b>
                        </button>
                    </div>
                    <div className="panel-body">
                        <div className="form">
                            <div className={inputClasses[FIELD_NAMES.DIRECTORY_NAME]}>
                                <label htmlFor="" className="input-label required">Project Directory: </label>
                                <input type="text" className='form-control' ref={(element) => {
                                    this.dirNameElement = element;
                                }} onChange={(evt) => {
                                    this.onFieldChange(FIELD_NAMES.DIRECTORY_NAME, evt);
                                }}/>
                                {
                                    this.state.fields[FIELD_NAMES.DIRECTORY_NAME].error ?
                                        <div
                                            className="error-message">{this.state.fields[FIELD_NAMES.DIRECTORY_NAME].error}</div>
                                        :
                                        null
                                }
                            </div>
                            <div className={inputClasses[FIELD_NAMES.PROJECT_NAME]}>
                                <label htmlFor="" className="input-label">Project Name: </label>
                                <input type="text" className='form-control' onChange={(evt) => {
                                    this.onFieldChange(FIELD_NAMES.PROJECT_NAME, evt);
                                }}/>
                            </div>
                            <div className={inputClasses[FIELD_NAMES.FILE_NAME]}>
                                <label htmlFor="" className="input-label">File Name: </label>
                                <input type="text" className='form-control' onChange={(evt) => {
                                    this.onFieldChange(FIELD_NAMES.FILE_NAME, evt);
                                }}/>
                            </div>
                            <div className={inputClasses[FIELD_NAMES.PROJECT_DESCRIPTION]}>
                                <label htmlFor="" className="input-label">Project Description: </label>
                                <textarea type="text" className='form-control' onChange={(evt) => {
                                    this.onFieldChange(FIELD_NAMES.PROJECT_DESCRIPTION, evt);
                                }}/>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer">
                        <button className="btn btn-danger" onClick={(evt) => {
                            this.close(evt);
                        }}>Cancel
                        </button>
                        <button className="btn btn-primary" onClick={(evt) => {
                            this.save(evt);
                        }}>Save
                        </button>
                    </div>
                </div>
            </Modal>
        )
    }
}

NewConfigModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    onFormSave: PropTypes.func.isRequired
};

export default NewConfigModal;