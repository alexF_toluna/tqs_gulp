import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import './styles.scss';

const ORIGIN_TYPES = {
    SERVER: 'Server',
    GULP: 'Gulp'
};

const MESSAGE_TYPES = {
    ERROR: 'ERROR',
    MESSAGE: 'Message'
};

const DISPLAY_MODE = {
    ALL: 0,
    SERVER: 1,
    GULP: 2
}

const getMessageItems = (data, mode) => {
    return data.map((msg, idx) => {
        let parsedMsg = JSON.parse(msg);
        let {origin, type, body, ts} = parsedMsg;
        let formattedMessage = `[${ts}]${origin || ORIGIN_TYPES.SERVER} ${type || MESSAGE_TYPES.MESSAGE}: ${body}`;

        const msgClasses = classnames({
            "text-danger": type === MESSAGE_TYPES.ERROR,
            "text-default": (type !== MESSAGE_TYPES.ERROR && origin === ORIGIN_TYPES.SERVER),
            "text-primary": (type !== MESSAGE_TYPES.ERROR && origin === ORIGIN_TYPES.GULP),
        });

        if (origin === ORIGIN_TYPES.SERVER && mode === 1 ||
            origin === ORIGIN_TYPES.GULP && mode === 2 ||
            mode === 0) {
            return (<p class={msgClasses} key={idx}>{formattedMessage}</p>);
        }
    });
};

const getErrorMessage = (disconnected) => {
    if (disconnected) {
        return (
            <div className="col-xs-12 text-danger">
                <i className="fa fa-warning"> </i>&nbsp;&nbsp;No connection with server !
            </div>
        );
    }

    return null;
};

let outputPanelContentElement = null;
let preElement = null;
let buttonsWrapperElement = null;

const onMouseMove = (evt) => {
    const preRect = preElement.getBoundingClientRect();
    const btnsRect = buttonsWrapperElement.getBoundingClientRect();

    if (evt.pageX > (preRect.left + preRect.width - btnsRect.width)) {
        $(outputPanelContentElement).addClass('buttons-visible');
    } else {
        $(outputPanelContentElement).removeClass('buttons-visible');
    }
};

const setStyles = () => {
    if (preElement && buttonsWrapperElement) {
        preElement.removeEventListener('mousemove', onMouseMove);
        preElement.addEventListener('mousemove', onMouseMove);
    }
};


class OutputPanelComponent extends Component {
    constructor(props) {
        super(props);
        this.name = 'OutputPanelComponent';


        this.state = {
            mode: 0
        };
    }

    render() {
        const {data} = this.props;

        const btn1 = classnames({
            'btn btn-default': true,
            'active': this.state.mode === 0
        });

        const btn2 = classnames({
            'btn btn-default': true,
            'active': this.state.mode === 1
        });

        const btn3 = classnames({
            'btn btn-default': true,
            'active': this.state.mode === 2
        });

        const preClasses = classnames({
            'disconnected': !this.props.connected,
            'connected': this.props.connected
        });

        return (
            <div className="container-fluid output-panel-wrapper">
                <div className="row">
                    <div className="col-xs-12 output-panel-content" ref={(element) => {
                        outputPanelContentElement = element;
                    }}>
                    <pre class={preClasses} ref={(element) => {
                        preElement = element;
                        this.props.setElement(element);

                        setStyles();
                    }}>
                        {getMessageItems(data, this.state.mode)}
                    </pre>
                        <div className="buttons-wrapper" ref={(element) => {
                            buttonsWrapperElement = element;
                            setStyles();
                        }}>
                            <div class="btn-group-vertical" role="group" aria-label="...">
                                <button type="button" class={btn1} onClick={() => {
                                    this.setState({mode: 0});
                                }}>All
                                </button>
                                <button type="button" class={btn2} onClick={() => {
                                    this.setState({mode: 1});
                                }}>Server
                                </button>
                                <button type="button" class={btn3} onClick={() => {
                                    this.setState({mode: 2});
                                }}>Gulp
                                </button>
                            </div>
                        </div>
                    </div>
                    {getErrorMessage(!this.props.connected)}
                </div>
            </div>
        );
    }
}

OutputPanelComponent.propTypes = {
    data: PropTypes.array.isRequired,
    setElement: PropTypes.func.isRequired,
    connected: PropTypes.bool.isRequired
};

module.exports = OutputPanelComponent;