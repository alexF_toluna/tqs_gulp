import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import styles from './styles.scss';

export default (props) => {
    console.log(props.params)
    return (
        <div id="page-not-found-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-6 col-xs-offset-3 text-center">
                        <div className="block-1">404</div>
                        <div className="block-2">Page not found</div>
                        <div className="block-3"><Link from={props.params} to="/dashboard">Go home</Link></div>
                    </div>
                </div>
            </div>
        </div>
    );
};