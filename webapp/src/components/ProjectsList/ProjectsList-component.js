import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import './styles.scss';

const ProjectsList = (props) => {
    const {projects, onWatchToggle} = props;

    //console.log('Rendering: ', projects);

    return (
        <ul class="list-group projects-list">
            {Object.keys(projects).map((project, idx) => {
                const wrapperClasses = classnames({
                    /*watch: projects[project]['watch'],*/
                    'bg-warning text-muted': !projects[project]['watch'] && !projects[project]['startup'],
                    'bg-success': (projects[project]['watch'] || projects[project]['startup']) && !(projects[project]['watch'] && projects[project]['startup']),
                    'bg-info': projects[project]['watch'] && projects[project]['startup']
                });

                const watchBtnClasses = classnames({
                    'btn btn-sm': true,
                    'btn-default': !projects[project]['watch'],
                    'btn-primary': projects[project]['watch']
                });

                const startupBtnClasses = classnames({
                    'btn btn-sm': true,
                    'btn-default': !projects[project]['startup'],
                    'btn-primary': projects[project]['startup']
                });


                return (
                    <li class="list-group-item" key={idx}>
                        <div class={wrapperClasses}>
                            <div className="text-container">
                                <b className="fa fa-caret-right"> </b>
                                <strong class="project-name">{project}</strong>
                                <span class="project-description">{projects[project]['projectDescription']}</span>
                            </div>
                            <div class="buttons-container">
                                <button title="Delete config" class="btn btn-sm btn-danger text-danger delete-btn" onClick={() => {
                                    props.onProjectDelete(project);
                                }}><i className="fa fa-close"> </i></button>
                                <button class={startupBtnClasses} title="Toggle startup state" onClick={() => {
                                    props.onStartupToggle(project);
                                }}>
                                    <b>S</b>
                                    {/*<strong class="fa fa-check"> </strong>*/}
                                </button>
                                <button class={watchBtnClasses} title="Toggle watch state" onClick={() => {
                                    props.onWatchToggle(project);
                                }}>
                                    <b>W</b>
                                    {/*<strong class="fa fa-check"> </strong>*/}
                                </button>
                            </div>
                        </div>
                    </li>
                );
            })}
        </ul>
    );
};

ProjectsList.propTypes = {
    projects: PropTypes.object.isRequired,
    onWatchToggle: PropTypes.func.isRequired,
    onStartupToggle: PropTypes.func.isRequired,
    onProjectDelete: PropTypes.func.isRequired
};

module.exports = ProjectsList;