import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute, browserHistory, Redirect} from 'react-router';
import styles from './styles.scss';
import App from './components/App/App-component';
import Dashboard from './components/Dashboard/Dashboard-component';
import PageNotFound from './components/PageNotFound/PageNotFound-component';
import store from './store';

import 'toastr/build/toastr.css';

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={App}>
                <IndexRoute component={Dashboard}/>
                <Route path="dashboard" component={Dashboard}>

                </Route>
                <Route path="*" component={PageNotFound} />
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app-root')
);
