const initialState = {
    ready: false
};

const APP_READY = 'app:APP_READY';

export const appReady = () => {
    return {
        type: APP_READY
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case APP_READY:
            return Object.assign(state, {ready: true});
            break;
        default:
            return state;
            break;
    }
};