import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import app from './app';

const rootReducer = combineReducers({
    app
});

let middleware;

if (!DEBUG) {
    middleware = applyMiddleware(thunk);
} else {
    middleware = applyMiddleware(thunk, logger());
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(
    middleware
);

const store = createStore(rootReducer, {}, enhancer);

export default store;