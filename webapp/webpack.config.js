const debug = process.env.NODE_ENV !== "production";
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const path = require('path');

const DEST_FOLDER = path.resolve(__dirname, 'dist');
const SRC_FOLDER = path.resolve(__dirname, 'src');

module.exports = {
    devtool: 'source-map',
    devServer: {
        historyApiFallback: true
    },
    entry: {
        'app': [SRC_FOLDER + '/main.js'],
        'vendor': [
            'react',
            'react-dom',
            'react-router',
            'redux',
            'redux-logger',
            'react-redux',
            'redux-thunk',
            'lodash',
            'moment/min/moment.min',
            'jquery-slimscroll',
            'toastr'
        ]
    },
    output: {
        path: DEST_FOLDER,
        publicPath: '/',
        filename: "[name].bundle.js"
    },
    module: {
        loaders: [
            {
                test: /(\.jsx|\.js)$/,
                include: /src/,
                exclude: /(node_modules|bower_components)/,
                loaders: ['jsx', 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0&plugins[]=react-html-attrs,plugins[]=transform-class-properties,plugins[]=transform-decorators-legacy']
            },
            {
                test: /\.scss$/,
                include: /src/,
                exclude: /(node_modules)/,
                loaders: ['style', 'css?url=false', 'resolve-url-loader', 'sass?sourceMap']
                /*loader: ExtractTextPlugin.extract(
                 'style', // The backup style loader
                 'css?sourceMap!postcss!sass?sourceMap'
                 )*/
            },
            {test: /\.css$/, exclude: / /, loaders: ['style', 'css']},
            {
                test: /\.html$/,
                include: /src/,
                loader: 'html'
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?name=./assets/fonts/[name]/[hash].[ext]&limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?name=./assets/fonts/[name]/[hash].[ext]&limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?name=./assets/fonts/[name]/[hash].[ext]&limit=10000&mimetype=application/octet-stream"
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file?name=./assets/fonts/[name]/[hash].[ext]"
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?name=./assets/fonts/[name]/[hash].[ext]&limit=10000&mimetype=image/svg+xml"
            },
        ]
    },
    plugins: [
        //new ExtractTextPlugin("[name].css")
        new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.js", Infinity),
        new CopyWebpackPlugin([
            {
                from: SRC_FOLDER + '/index.html',
                to: DEST_FOLDER + '/index.html'
            }, {
                from: SRC_FOLDER + '/assets/',
                to: DEST_FOLDER + '/assets/'
            },{
                from: SRC_FOLDER + '/favicon.ico',
                to: DEST_FOLDER + '/favicon.ico'
            }]),
        new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
        new ExtractTextPlugin('styles.css'),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            sourceMap: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true,
                warnings: true,
                drop_console: true
            },
            comments: false
        }),
        new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            host: 'localhost',
            port: 54321,
            proxy: {
                target: 'localhost:54345',
                ws: true
            },
            files: [
                'webapp/dist/**/*'
            ]
            /*server: { baseDir: ['dist'] }*/
        })
    ]
};